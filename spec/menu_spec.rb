require 'spec_helper'


describe Dieta do
    before :each do
        @p1 = Dieta.new("Bocadillo de plomo", 700, 30, 30, 30, 40)
        @p2 = Plato.new("Pan con plomo", "1 porcion", 300)
        @p1.setPlato("Pan con plomo", "1 porcion", 300)
        @p1.setPlato("yogurt", "1 unidad", 30)
    end
    describe "constructor" do
        it "Se almacena el titulo" do
            expect(@p1.titulo).to eq("Bocadillo de plomo")
        end
        it "Se almacena el vct" do
            expect(@p1.vct).to eq(700)
        end
        it "Se almacena la ingesta" do
            expect(@p1.ingesta).to eq(30)
        end
          it "La clase Plato almacena descripcion" do
            expect(@p2.descripcion).to eq("Pan con plomo")
        end
        it "La clase Plato almacena los gramos" do
            expect(@p2.gramos).to eq(300)
        end
        it "La clase Plato almacena la porcion" do
            expect(@p2.porcion).to eq("1 porcion")
        end
    end
    
    describe "El resto de datos" do
        it "La clase dieta tiene porcentaje de proteinas" do
            expect(@p1.proteinas).to eq(30)
        end
        it "La clase dieta tiene porcentaje de grasas" do
            expect(@p1.grasas).to eq(30)
        end
        it "La clase dieta tiene porcentaje de hidratos" do
            expect(@p1.hidratos).to eq(40)
        end
        it "La clase dieta tiene que tener un conjunto de platos" do
            expect(@p1.platos.length).to eq(2)
        end
        it "Pidiendo el primer plato" do
            expect(@p1.platos[0].descripcion).to eq("Pan con plomo")
        end
        it "Pidiendo el primer plato" do
            expect(@p1.platos[1].descripcion).to eq("yogurt")
        end
    end
    describe "Formateo de datos" do
        it "La clase se muestra tal y como debe" do
            expect(@p1.to_s).to eq("Bocadillo de plomo (30%)\n- Pan con plomo, 1 porcion, 300 g\n- yogurt, 1 unidad, 30 g\nV.C.T. | % 700 kcal | 30% - 30% - 40%")
        end
    end
end

describe Node do
    before :each do
        @p1 = Dieta.new("Desayuno", 200, 5, 7, 10, 5)
        @p1.setPlato("Pan con queso", "1 porcion", 300)
        @p2 = Dieta.new("Almuerzo", 500, 20, 15, 20, 45)
        @p2.setPlato("Papas", "4 porcion", 100)
        @p2.setPlato("Solomillo", "1 porcion", 300 )
        @n1 = Node.new(@p1,@p2)
    end
    
    describe "Pruebas de la clase node" do
        it "Un nodo debe tener un atributo con el valor" do
            expect(@n1.value.titulo).to eq("Desayuno")
        end
        it "Un nodo debe tener un atributo con el next" do
            expect(@n1.next.titulo).to eq("Almuerzo")
        end
    end
end
describe List do
     before :each do
        @p1 = Dieta.new("Desayuno",  288.0, 15, 17, 21, 62)
        @p1.setPlato("Leche desnatada", "1 vaso", 200)
        @p1.setPlato("Cacao instantaneo", "1 c/sopera", 10)
        @p1.setPlato("Cereales de desayuno en hojuelas", "1 bol pequeño", 40)
        @p1.setPlato("Almendras laminadas (10 unidades)", "2 c/soperas", 10)
        
        @p2 = Dieta.new("Media Mañana", 255.5, 10, 7, 24, 69)
        @p2.setPlato("Cerezas", "10-12 unidades medianas", 120)
        @p2.setPlato("Galletas bifidus con sesamo", "4 unidades", 40 )
        
        @p3 = Dieta.new("Almuerzo",   785.9, "30-35", 19, 34, 47)
        @p3.setPlato("Macarrones con salsa de tomate y queso parmesano", "1 1/2 cucharon", 200)
        @p3.setPlato("Escalope de ternera", "1 bistec mediano", 100)
        @p3.setPlato("Ensalada basica con zanahoria rallada", "guarnicion de 120", 120)
        @p3.setPlato("Mandarina", "1 grande", 180)
        @p3.setPlato("Pan de trigo integral", "1 rodaja", 20)
        
        @p4 = Dieta.new("Merienda", 313.6, 15, 10, 30, 60)
        @p4.setPlato("Galletas de leche con chocolate y yogur", "4 unidades", 46)
        @p4.setPlato("Flan de vainilla sin huevo", "1 unidades", 110 )
        
        @p5 = Dieta.new("Cena", 561.6, "25-30", 19, 40, 41)
        @p5.setPlato("Crema de bubango", "2 cucharones", 200)
        @p5.setPlato("Tortilla campesina con espinacas", " 1 cuña grande", 150)
        @p5.setPlato("Tomate en dados con atun", " 5 a 6 c/soperas", 150)
        @p5.setPlato("Piña natural o en su jugo picada", "5 c/soperas", 120)
        @p5.setPlato("Pan de trigo integral", "1 rodaja", 20)
        
        
        @n1 = Node.new(@p1, nil)
        @n2 = Node.new(@p2, nil)
        @n3 = Node.new(@p3, nil)
        @n4 = Node.new(@p4, nil)
        @n5 = Node.new(@p5, nil)
        
        @l1 = List.new
        @l1.insertar(@n1)
        @l1.insertar(@n2)
        @l1.insertar(@n3)
        @l1.insertar(@n4)
        @l1.insertar(@n5)
        
    end
    
    describe "Pruebas de las clase Lista" do
         it "Se puede insertar y extraer un elemento" do
            expect(@l1.extraer.value.titulo).to eq("Cena") 
         end
         it "Extrayendo muchos elementos" do
            expect(@l1.extraer.value.to_s).to eq(@n5.value.to_s)
            expect(@l1.extraer.value.to_s).to eq(@n4.value.to_s)
            expect(@l1.extraer.value.to_s).to eq(@n3.value.to_s)
            expect(@l1.extraer.value.to_s).to eq(@n2.value.to_s)
            expect(@l1.extraer.value.to_s).to eq(@n1.value.to_s)
         end
         it "Debe existir una cabecera en la lista" do
            expect(@l1.cabecera.value.to_s).to eq(@n5.value.to_s)
            
         end
    end
end
    
    describe Node_double do
        before :each do
            @p1 = Dieta.new("Desayuno", 200, 5, 7, 10, 5)
            @p1.setPlato("Pan con queso", "1 porcion", 300)
            @p2 = Dieta.new("Almuerzo", 500, 20, 15, 20, 45)
            @p2.setPlato("Papas", "4 porcion", 100)
            @p2.setPlato("Solomillo", "1 porcion", 300 )
            @p3 = Dieta.new("Cena", 200, 10, 15, 30, 35)
            @p3.setPlato("Pasta", "1 racion", 100)
            @n1 = Node_double.new(@p2,@p3,@p1)
         end
         describe "Pruebas de la clase node" do
            it "Un nodo debe tener un atributo con el valor" do
                expect(@n1.value.titulo).to eq("Almuerzo")
            end
            it "Un nodo debe tener un atributo con el next" do
                expect(@n1.next.titulo).to eq("Cena")
            end
            
             it "Un nodo debe tener un atributo con el prev" do
                expect(@n1.prev.titulo).to eq("Desayuno")
            end
    end
end
    
    describe List_double do
        before :each do
            @p1 = Dieta.new("Desayuno",  288.0, 15, 17, 21, 62)
            @p1.setPlato("Leche desnatada", "1 vaso", 200)
            @p1.setPlato("Cacao instantaneo", "1 c/sopera", 10)
            @p1.setPlato("Cereales de desayuno en hojuelas", "1 bol pequeño", 40)
            @p1.setPlato("Almendras laminadas (10 unidades)", "2 c/soperas", 10)
        
            @p2 = Dieta.new("Almuerzo",   785.9, "30-35", 19, 34, 47)
            @p2.setPlato("Macarrones con salsa de tomate y queso parmesano", "1 1/2 cucharon", 200)
            @p2.setPlato("Escalope de ternera", "1 bistec mediano", 100)
            @p2.setPlato("Ensalada basica con zanahoria rallada", "guarnicion de 120", 120)
            @p2.setPlato("Mandarina", "1 grande", 180)
            @p2.setPlato("Pan de trigo integral", "1 rodaja", 20)
        
            @p3 = Dieta.new("Cena", 561.6, "25-30", 19, 40, 41)
            @p3.setPlato("Crema de bubango", "2 cucharones", 200)
            @p3.setPlato("Tortilla campesina con espinacas", " 1 cuña grande", 150)
            @p3.setPlato("Tomate en dados con atun", " 5 a 6 c/soperas", 150)
            @p3.setPlato("Piña natural o en su jugo picada", "5 c/soperas", 120)
            @p3.setPlato("Pan de trigo integral", "1 rodaja", 20)
        
            @l1 = List_double.new
            @l1.insertarp(@p1)
            @l1.insertarf(@p2)
            @l1.insertarf(@p3)
            
        end
        
          describe "Pruebas de la clase lista doble" do
        
            it "Se puede insertar y extraer por el principio" do
                expect(@l1.extraerp.value.titulo).to eq("Desayuno") 
            end
            
            it "Se puede insertar y extraer por el final" do
                expect(@l1.extraerf.value.titulo).to eq("Cena") 
            end
            
        end
    end
    describe Dieta do
        before :each do
            @p1 = Dieta.new("Desayuno",  288.0, 15, 17, 21, 62)
            @p1.setPlato("Leche desnatada", "1 vaso", 200)
            @p1.setPlato("Cacao instantaneo", "1 c/sopera", 10)
            
            
            @plato1 = Plato.new("Leche desnatada", "1 vaso", 200)
            @p1ato2 = Plato.new("Cacao instantaneo", "1 c/sopera", 10)
            @platos1 = [@plato1, @plato2]
            @p2 = Dieta_alimento.new("Desayuno", 288.0, 15, 17, 21, 62, @platos1, "leche, huevos, pescado, carne y frutos secos")
            
            @p3 = Dieta_edad.new("Desayuno",  288.0, 15, 17, 21, 62, @platos1, "9 a 13 años" )
        end
        
     describe "Pruebas de grupos ,tipos y clases" do
         it "Comprobacion que la nueva clase Dieta_alimento funciona" do
             expect(@p2.grupo).to eq("leche, huevos, pescado, carne y frutos secos")
         end
         
         it "Comprobacion que la nueva clase Dieta_edad funciona" do
             expect(@p3.grupo).to eq("9 a 13 años")
         end
     end
     describe "Pruebas para comprobar que la clase de los objetos" do
         it "Comprobacion de pertenencia de clase" do  
            expect(@p1.instance_of?Dieta).to eq(true)
            expect(@p2.instance_of?Dieta_alimento).to eq(true)
            expect(@p3.instance_of?Dieta_edad).to eq(true)
        end
     end
     
     describe "Pruebas para comprobar el tipo de los objetos" do
         it "Comprobacion de tipo" do  
            expect(@p1.respond_to?'to_s').to eq(true)
            expect(@p2.respond_to?'get_grupo_alimentos').to eq(true)
            expect(@p3.respond_to?'get_grupo_edad').to eq(true)
        end
     end
     
     describe "Pruebas para comprobar la jerarquia" do
         it "Comprobacion de pertenencia la jerarquia para Dieta" do  
            expect(@p1.is_a?Dieta).to eq(true)
            expect(@p1.is_a?Object).to eq(true)
            expect(@p1.is_a?BasicObject).to eq(true)
            expect(@p1.is_a?Dieta_alimento).to eq(false)
            expect(@p1.is_a?Dieta_edad).to eq(false)
        end
        it "Comprobacion de pertenencia la jerarquia para Dieta_alimento" do  
            expect(@p2.is_a?Dieta_alimento).to eq(true)
            expect(@p2.is_a?Dieta).to eq(true)
            expect(@p2.is_a?Object).to eq(true)
            expect(@p2.is_a?BasicObject).to eq(true)
            expect(@p2.is_a?Dieta_edad).to eq(false)
        end
        it "Comprobacion de pertenencia la jerarquia para Dieta_edad" do  
            expect(@p3.is_a?Dieta_edad).to eq(true)           
            expect(@p3.is_a?Dieta).to eq(true)
            expect(@p3.is_a?Object).to eq(true)
            expect(@p3.is_a?BasicObject).to eq(true)
            expect(@p3.is_a?Dieta_alimento).to eq(false)
        end
     end
end





