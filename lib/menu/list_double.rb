class List_double
    attr_accessor :principio, :final
     
    def initialize
        @principio = nil
        @final = nil
    end
    
    def insertarp(nodo)
         n = Node_double.new(nodo, nil, nil)
         n.next = @principio
         n.prev = nil
         if(@final == nil) then
             @final = n
         else
             @principio.prev = n;
         end
         @principio = n
    end
    
    def insertarf(nodo)
         n = Node_double.new(nodo, nil, nil)
         n.prev = @final
         n.next = nil

        if(@final == nil) then
            @principio = n
        else
            @final.next = n
        end
        @final = n
    end
    
    def extraerp
       aux = @principio
       @principio = @principio.next
       
       if(@principio == nil) then 
        @final = nil
       end
       return aux
    end
    
    def extraerf
        aux = @final
        @final = @final.prev
        
        if(@final == nil) then
            @principio = nil
        else
            @final.next = nil
        end
        return aux 
    end
    
end