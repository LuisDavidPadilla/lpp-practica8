#require "/home/ubuntu/workspace/LPP/prct06/tdd-menu-lpp-25/lib/menu/node.rb"
class List
    attr_accessor :cabecera
    
    def initialize()
        @cabecera = nil
    end
    
    def insertar(nodo)
        n = Node.new(nodo.value, nodo.next)
        n.next = @cabecera
        @cabecera = n
    end
    
    def extraer
        aux = cabecera
        @cabecera = @cabecera.next
        return aux
        
    end
    
end
