require './lib/menu/plato.rb'
class Dieta
    attr_accessor :titulo, :vct, :ingesta, :platos, :proteinas, :grasas, :hidratos
    
    def initialize(receta, vct, ingesta, proteinas, grasas, hidratos)
        @titulo = receta
        @vct = vct
        @ingesta = ingesta
        @proteinas = proteinas
        @grasas = grasas
        @hidratos = hidratos
        @platos = []
    end
    
    def setPlato(descripcion, porcion, gramos)
        @platos << Plato.new(descripcion, porcion, gramos)
    end
    
    def to_s
        s = "#{titulo} (#{ingesta}%)\n"
        platos.each do |comida|
            s << "- #{comida.descripcion}, #{comida.porcion}, #{comida.gramos} g\n" 
        end
        s << "V.C.T. | % #{vct} kcal | #{proteinas}% - #{grasas}% - #{hidratos}%"
    end
    
end

class Dieta_alimento < Dieta
    attr_accessor :receta, :vct, :ingesta, :proteinas, :grasas, :hidratos, :platos, :grupo
    def initialize(receta, vct, ingesta, proteinas, grasas, hidratos, platos, grupo)
        super(receta, vct, ingesta, proteinas, grasas, hidratos)
        @platos = platos
        @grupo = grupo
    end    
    
    def get_grupo_alimentos
        s = @grupo
        return s
    end
end

class Dieta_edad < Dieta
    attr_accessor :receta, :vct, :ingesta, :proteinas, :grasas, :hidratos, :platos, :grupo
    def initialize(receta, vct, ingesta, proteinas, grasas, hidratos, platos, grupo)
        super(receta, vct, ingesta, proteinas, grasas, hidratos)
        @platos = platos
        @grupo = grupo
    end    
    
    def get_grupo_edad
        s = @grupo
        return s
    end
end

