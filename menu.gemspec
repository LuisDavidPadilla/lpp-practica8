# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'menu/version'

Gem::Specification.new do |spec|
  spec.name          = "menu"
  spec.version       = Menu::VERSION
  spec.authors       = ["Carlos Castro Garcia", "Luis David Padilla Martin"]
  spec.email         = ["alu0100819847@ull.edu.es", "alu0100816518@ull.edu.es"]

  spec.summary       = %q{Practica 6}
  spec.description   = %q{Desarrollar una gema con un menu}
  spec.homepage      = "https://github.com/ULL-ESIT-LPP-1617/tdd-menu-lpp-25.git"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://github.com/ULL-ESIT-LPP-1617/tdd-menu-lpp-25.git"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "guard"
  spec.add_development_dependency "guard-rspec"
  spec.add_development_dependency "guard-bundler"

end
